import { dbField, dbTable } from '@viatsyshyn/ts-orm';

export const UQ_TOKEN = 'uq_token';

@dbTable('user_fcm', [{
  name: UQ_TOKEN,
  fields: ['token']
}])
export class UserFCMModel {

  @dbField({
    name: 'platform',
    primaryKey: true,
  })
  public platform: string;

  @dbField({
    name: 'deviceId',
    primaryKey: true,
  })
  public deviceId: string;

  @dbField()
  public userId: number;

  @dbField()
  public token: string;

  @dbField({
    name: 'creationdate',
    readonly: true,
  })
  public created: Date;

  @dbField({
    name: 'lastmodified',
    readonly: true,
  })
  public modified: Date;
}
