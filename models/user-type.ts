export enum UserType {
  Unknown = 0,
  Admin = 1,
  User = 2,
  Guest = 3,
}
