import { dbField, dbTable } from '@viatsyshyn/ts-orm';

export interface IAttribute {
  name: string;
  title: string;
  scale: string;
  precision?: number;
  monotone?: boolean;
  expectedMin?: number;
  expectedMax?: number;
  lambda?: string;
  sign?: boolean;
  virtual?: boolean;
}

export interface IListenSetting {
  kind: 'listen';
  attribute: string;
}

export interface IRepublishSetting {
  kind: 'republish';
  attribute: string;
}

export interface IScheduleSetting {
  kind: 'schedule';
  schedule: {[key: string]: string|number|boolean};
}

export type ISetting = IRepublishSetting | IScheduleSetting | IListenSetting;

export const UQ_ATTRIBUTE_UID = 'uq_attribute_uid';
export const UQ_ATTRIBUTE_KEY = 'uq_attribute_key';

@dbTable('attribute', [
  {
    name: UQ_ATTRIBUTE_UID,
    fields: ['uid'],
    default: true,
  }, {
    name: UQ_ATTRIBUTE_KEY,
    fields: ['device', 'key', 'kind']
  }
])
export class AttributeModel {

  @dbField({
    autoIncrement: true,
    primaryKey: true,
    readonly: true,
  })
  public id: number;

  @dbField({
    uid: true,
    readonly: true,
  })
  public uid: string;

  @dbField()
  public device: number;

  @dbField()
  public key: string;

  @dbField()
  public title: string;

  @dbField()
  public kind: 'in' | 'out';

  @dbField({json: true})
  public definition: IAttribute;

  @dbField({json: true})
  public setting: ISetting | null;

  @dbField({
    name: 'creationdate',
    readonly: true,
  })
  public created: Date;

  @dbField({
    name: 'lastmodified',
    readonly: true,
  })
  public modified: Date;
}
