import { dbField, dbTable, dbManyField } from '@viatsyshyn/ts-orm';
import { UserType } from './user-type';

export const UQ_USER_UID = 'uq_user_uid';
export const UQ_USERNAME = 'uq_username';
export const UQ_EMAIL = 'uq_email';

@dbTable('user', [
  {
    name: UQ_USER_UID,
    fields: ['uid'],
    default: true,
  }, {
    name: UQ_USERNAME,
    fields: ['username'],
  }, {
    name: UQ_EMAIL,
    fields: ['email'],
  }
])
export class UserModel {

  @dbField({
    autoIncrement: true,
    primaryKey: true,
    readonly: true,
  })
  public id: number;

  @dbField({
    uid: true,
    readonly: true,
  })
  public uid: string;

  @dbField('username')
  public userName: string;

  @dbField({
    sensitive: true,
  })
  public password: string;

  @dbField()
  public email?: string | null;

  @dbField({
    defaults: UserType.Unknown,
  })
  public type: UserType;

  @dbField({
    name: 'creationdate',
    readonly: true,
  })
  public created: Date;

  @dbField({
    name: 'lastmodified',
    readonly: true,
  })
  public modified: Date;
}
