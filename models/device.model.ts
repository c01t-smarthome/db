import { dbField, dbTable } from '@viatsyshyn/ts-orm';
import {IAttribute} from './attribute.model';

export const UQ_DEVICE_UID = 'uq_device_uid';
export const UQ_DEVICE_TAG = 'uq_device_tag';

export interface IValueAction {
  kind: 'value';
  attribute: string;
  value: any;
}
export interface IToggleAction {
  kind: 'toggle';
  attribute: string;
}

export interface IWidget {
  prime: string | IAttribute;
  secondary?: string | IAttribute;
  state?: string | IAttribute;
  action?: IValueAction | IToggleAction;
}

@dbTable('device', [
  {
    name: UQ_DEVICE_UID,
    fields: ['uid'],
    default: true,
  }, {
    name: UQ_DEVICE_TAG,
    fields: ['tag'],
  }
])
export class DeviceModel {

  @dbField({
    autoIncrement: true,
    primaryKey: true,
    readonly: true,
  })
  public id: number;

  @dbField({
    uid: true,
    readonly: true,
  })
  public uid: string;

  @dbField()
  public tag: string;

  @dbField()
  public name: string;

  @dbField()
  public description: string;

  @dbField()
  public firmware: string;

  @dbField()
  public zone: number | null;

  @dbField({
    sensitive: true,
    defaults: false,
  })
  public forgotten: boolean;

  @dbField({json: true})
  public icons: {[key: string]: string};

  @dbField({json: true})
  public widgets: {icon: IWidget, device: IWidget[]};

  @dbField({
    name: 'creationdate',
    readonly: true,
  })
  public created: Date;

  @dbField({
    name: 'lastmodified',
    readonly: true,
  })
  public modified: Date;
}
