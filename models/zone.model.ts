import { dbField, dbTable } from '@viatsyshyn/ts-orm';

export const UQ_ZONE_UID = 'uq_zone_uid';
export const UQ_ZONE_TAG = 'uq_zone_tag';

@dbTable('zone', [
  {
    name: UQ_ZONE_UID,
    fields: ['uid'],
    default: true,
  }, {
    name: UQ_ZONE_TAG,
    fields: ['tag']
  }
])
export class ZoneModel {

  @dbField({
    autoIncrement: true,
    primaryKey: true,
    readonly: true,
  })
  public id: number;

  @dbField({
    uid: true,
    readonly: true,
  })
  public uid: string;

  @dbField()
  public name: string;

  @dbField()
  public tag: string;

  @dbField()
  public color?: string;

  @dbField({
    name: 'creationdate',
    readonly: true,
  })
  public created: Date;

  @dbField({
    name: 'lastmodified',
    readonly: true,
  })
  public modified: Date;
}
