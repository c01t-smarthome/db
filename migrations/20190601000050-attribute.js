'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

const sqlUp = `
-- -----------------------------------------------------
-- Table "attribute"
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "attribute" (
  "id" SERIAL PRIMARY KEY,
  "uid" UUID NOT NULL DEFAULT uuid_generate_v4(),
  "device" integer NOT NULL,
  "key" VARCHAR(45) NOT NULL,
  "title" VARCHAR(255) NOT NULL,
  "kind" VARCHAR(10) NOT NULL,
  "definition" VARCHAR(4096) NOT NULL,
  "setting" VARCHAR(1024) NULL,
  "lastmodified" timestamp NOT NULL DEFAULT current_timestamp,
  "creationdate" timestamp NOT NULL DEFAULT current_timestamp
);

ALTER TABLE "attribute" ADD CONSTRAINT "uq_attribute_uid" UNIQUE ("uid");
ALTER TABLE "attribute" ADD CONSTRAINT "uq_attribute_key" UNIQUE ("device", "key", "kind");
ALTER TABLE "attribute" ADD CONSTRAINT "fk_attribute_device" FOREIGN KEY ("device") REFERENCES "device" ("id");

CREATE TRIGGER "update_attribute_lastmodified"
    BEFORE UPDATE ON "attribute"
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE update_lastmodified_column();
`;

exports.up = function (db) {
  return db.runSql(sqlUp);
};


const sqlDown = `
DROP TRIGGER IF EXISTS "update_device_lastmodified" ON "zone";
DROP TABLE IF EXISTS "device";
`;

exports.down = function (db) {
  return db.runSql(sqlDown);
};

exports._meta = {
  "version": 1
};
