'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

const sqlUp = `
CREATE TABLE IF NOT EXISTS "user_fcm" (
  "userId" integer NOT NULL,
  CONSTRAINT "fk_user_fcm_user"
    FOREIGN KEY ("userId")
    REFERENCES "user" ("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  "platform" VARCHAR(45) NOT NULL,
  "deviceId" VARCHAR(45) NOT NULL,
  PRIMARY KEY ("platform", "deviceId"),
  "token" VARCHAR(256) NOT NULL CONSTRAINT "uq_token" UNIQUE,
  "lastmodified" timestamp NOT NULL DEFAULT current_timestamp,
  "creationdate" timestamp NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER "update_user_fcm_lastmodified"
    BEFORE UPDATE ON "user_fcm"
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE update_lastmodified_column();
`;

exports.up = function (db) {
  return db.runSql(sqlUp);
};

const sqlDown = `
DROP TRIGGER IF EXISTS "update_user_fcm_lastmodified" ON "user_fcm";
DROP TABLE IF EXISTS "user_fcm";
`;

exports.down = function (db) {
  return db.runSql(sqlDown);
};

exports._meta = {
  "version": 1
};
