'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

const sqlUp = `
-- -----------------------------------------------------
-- Table "zone"
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "zone" (
  "id" SERIAL PRIMARY KEY,
  "uid" UUID NOT NULL DEFAULT uuid_generate_v4(),
  "tag" VARCHAR(45) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "color" VARCHAR(255) NULL,
  "lastmodified" timestamp NOT NULL DEFAULT current_timestamp,
  "creationdate" timestamp NOT NULL DEFAULT current_timestamp
);

ALTER TABLE "zone" ADD CONSTRAINT "uq_zone_uid" UNIQUE ("uid");
ALTER TABLE "zone" ADD CONSTRAINT "uq_zone_tag" UNIQUE ("tag");

CREATE TRIGGER "update_zone_lastmodified"
    BEFORE UPDATE ON "zone"
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE update_lastmodified_column();
`;

exports.up = function (db) {
  return db.runSql(sqlUp);
};


const sqlDown = `
DROP TRIGGER IF EXISTS "update_zone_lastmodified" ON "zone";
DROP TABLE IF EXISTS "zone";
`;

exports.down = function (db) {
  return db.runSql(sqlDown);
};

exports._meta = {
  "version": 1
};
