'use strict';

let dbm;
let type;
let seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

const sqlUp = `
  CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

  CREATE OR REPLACE FUNCTION update_lastmodified_column()
  RETURNS TRIGGER AS $$
  BEGIN
    NEW.lastmodified = now(); 
    RETURN NEW;
  END;
  $$ language 'plpgsql';
`;

exports.up = function (db) {
  return db.runSql(sqlUp);
};


const sqlDown = `
  DROP FUNCTION IF EXISTS "update_lastmodified_column" RESTRICT;
`;

exports.down = function (db) {
  return db.runSql(sqlDown);
};

exports._meta = {
  "version": 1
};
