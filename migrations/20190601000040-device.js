'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

const sqlUp = `
-- -----------------------------------------------------
-- Table "device"
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "device" (
  "id" SERIAL PRIMARY KEY,
  "uid" UUID NOT NULL DEFAULT uuid_generate_v4(),
  "tag" VARCHAR(45) NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "description" VARCHAR(255) NULL,
  "firmware" VARCHAR(255) NULL,
  "zone" integer NULL,
  "forgotten" BOOLEAN NOT NULL DEFAULT False,
  "icons" VARCHAR(102400) NOT NULL,
  "widgets" VARCHAR(10240) NOT NULL,
  "lastmodified" timestamp NOT NULL DEFAULT current_timestamp,
  "creationdate" timestamp NOT NULL DEFAULT current_timestamp
);

ALTER TABLE "device" ADD CONSTRAINT "uq_device_uid" UNIQUE ("uid");
ALTER TABLE "device" ADD CONSTRAINT "uq_device_tag" UNIQUE ("tag");
ALTER TABLE "device" ADD CONSTRAINT "fk_device_zone" FOREIGN KEY ("zone") REFERENCES "zone" ("id");

CREATE TRIGGER "update_device_lastmodified"
    BEFORE UPDATE ON "device"
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE update_lastmodified_column();
`;

exports.up = function (db) {
  return db.runSql(sqlUp);
};


const sqlDown = `
DROP TRIGGER IF EXISTS "update_device_lastmodified" ON "zone";
DROP TABLE IF EXISTS "device";
`;

exports.down = function (db) {
  return db.runSql(sqlDown);
};

exports._meta = {
  "version": 1
};
