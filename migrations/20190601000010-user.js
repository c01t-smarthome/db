'use strict';

let dbm;
let type;
let seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

const sqlUp = `
-- -----------------------------------------------------
-- Table "user"
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "user" (
  "id" SERIAL PRIMARY KEY,
  "uid" UUID NOT NULL DEFAULT uuid_generate_v4(),
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(255) NOT NULL,
  "email" VARCHAR(255) NULL,
  "type" smallint NOT NULL,
  "lastmodified" timestamp NOT NULL DEFAULT current_timestamp,
  "creationdate" timestamp NOT NULL DEFAULT current_timestamp
);

ALTER TABLE "user" ADD CONSTRAINT "uq_user_uid" UNIQUE ("uid");
ALTER TABLE "user" ADD CONSTRAINT "uq_username" UNIQUE ("username");
ALTER TABLE "user" ADD CONSTRAINT "uq_email" UNIQUE ("email");

CREATE TRIGGER "update_user_lastmodified"
    BEFORE UPDATE ON "user"
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE update_lastmodified_column();
`;

exports.up = function (db) {
  return db.runSql(sqlUp);
};


const sqlDown = `
DROP TRIGGER IF EXISTS "update_user_lastmodified" ON "user";
DROP TABLE IF EXISTS "user";
`;

exports.down = function (db) {
  return db.runSql(sqlDown);
};

exports._meta = {
  "version": 1
};
