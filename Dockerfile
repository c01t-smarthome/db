ARG VERSION_ID

FROM postgres:10

ENV VERSION_ID $VERSION_ID

# install node
RUN apt-get update && apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get update && apt-get install -y nodejs
RUN rm -rf /var/lib/apt/lists/*

RUN npm install -g db-migrate db-migrate-pg

RUN mkdir -p /srv
WORKDIR /srv

#include files
COPY . .

COPY run-migrations.sh /usr/local/bin
RUN chmod +x /usr/local/bin/run-migrations.sh

COPY docker-entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
